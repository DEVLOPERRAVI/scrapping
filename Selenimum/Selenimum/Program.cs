﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;  ///  
using OpenQA.Selenium.Chrome;   /// 
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Drawing;
using System.Collections.Specialized;

namespace Selenimum
{
    class Program
    {
        #region  For Varaiables
        public static int count = 0; // To Maintain how many times pages is scrolled 
        public static int replypos = 0; // To  get the  position of the reply so that the content is made according to it.
        public static int retweetpos = 0;  // To get the Retweet position 
        public static int likepos = 0;  // To get the like position 
        public static int maxcount = 0; // To  get the count of the maximum number of the substrings a single tweet is divoded depending on Break line 
        public static string url = "";  // For Full Url
        public static string profile = "";  // To take  profile 
        public static string atprofile = ""; // To take Value from @ profile naame 
        public static string profilename = "";  // To take the name of the profilename
        public static int retweetedpos = 0; // To  get the retweeted  position of the more so that the content is  made according to it.
        public static bool postedtoday = false; //  To check whether it is posted today or not if it is Yes then today's Date is addded.
        public static int morepos = 0; // To Find out the mor pos
        public static bool hashtag = true;
        public static int Case = 0;
        public static bool erroroccured = false;
        // 0  for  normal tweet   if  the tweet is orgianlly posted from same account  
        //1   for  retweeted      if  they rtweeted the same content  twwet orginally posted from different account 
        //2  for reply            if the they did reply to the some others tweet 
        //3  For Pinned Tweet 
        //4 In reply With tweet but orgainial tweet will not be there 
        // 5 Special case occured
        public static int lastfilledvalue = 0;// For Header account 
        public static int lastfilledtweetvalue = 1;
        public static int profilepos = 0;// In order to maintain the exact position of the old Account  Name 
        object misValue = System.Reflection.Missing.Value;
        public static bool Addyear = false;
        public static bool tempsetting = false;
        public static string input = "";
        public static int sitevalue = 0; // 0 For Twitter 
                                         // 1 For FaceBook 
                                         // 2 For Trip Adivosor 
                                         //3 Reviews From Trip Advisor 
                                         // 4 For processing Airbnb Excel Data 
                                         //5 For Checking whether the Porperty name is present in Yelp 
                                         // 6 For Getting the Cities name Data from Inside AirBNB Data    // YAHOO FINANCE   
                                         // 7 To  Find the Price Range of the Restaurant 
                                         // For Yelp Reviews 
        public static bool yf = true;
        public static int valuelast = 0;
        public static StringCollection myCol = new StringCollection();
        //string[] ammenitiescollection = new string[] { "TV", "Cable TV", "Internet", "Wireless Internet", "Wheelchair accessible", "Kitchen", "Gym", "Breakfast", "Elevator in building", "Hot tub", "Indoor fireplace", "Buzzer/wireless intercom", "Heating", "Family/kid friendly", "Washer", "Dryer", "Smoke detector", "Carbon monoxide detector", "First aid kit", "Safety card", "Fire extinguisher", "Essentials", "Shampoo", "24-hour check-in", "Hangers", "Hair dryer", "Iron", "Laptop friendly workspace", "Self Check-In", "Lockbox", "Private entrance" };
        string[] ammenitiescollection = new string[] { };


        #endregion

        static void Main()
        {
            Excel.Application xlApp = new Excel.Application();
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor; /// Explain 
            Excel.Workbook OutputxlWorkBook; // Output 
            xlApp = new Excel.Application();
            object misValue = System.Reflection.Missing.Value;
            OutputxlWorkBook = xlApp.Workbooks.Add(misValue);
            OutputxlWorkBook.Worksheets.Add();
            if (sitevalue != 6) //  OTHER THAN INSIDE AIRBNB
            {
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@"inputtwitter2.xlsx"); // Read Input file and process for scrapping 
                Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;
                for (int i = 2; i < 3; i++)
                // for (int i = 2; i < 3; i++) // TEMP SETTING TO PROCESS RECORD ONLY ONCE 
                {
                    valuelast = i;
                    try
                    {
                        if (sitevalue == 4)
                            url = (string)(xlRange.Cells[i, 2] as Excel.Range).Value2;
                        else if (sitevalue == 5)
                            url = (string)(xlRange.Cells[i, 6] as Excel.Range).Value2;
                        else
                            url = (string)(xlRange.Cells[i, 1] as Excel.Range).Value2;
                        if (url != null)// Hotel Name 
                        {
                            if (sitevalue == 2) // TRIP ADIVISOR 
                            {
                                lastfilledtweetvalue = lastfilledtweetvalue + 1; /// To give Two empty Spaces before staring new profile 
                                ExcelTripAdivsorMain(OutputxlWorkBook);

                            }
                            else if (sitevalue == 3) // TRIP ADIVISOR  Reviews
                            {
                                lastfilledtweetvalue = lastfilledtweetvalue + 1; /// To give Two empty Spaces before staring new profile 
                                ExcelTripReviewAdivsorMain(OutputxlWorkBook);

                            }
                            else if (sitevalue == 4) // AIR BNB
                            {
                                if (url != "{}")
                                {
                                    int rowid = (int)(xlRange.Cells[i, 1] as Excel.Range).Value2;
                                    lastfilledtweetvalue = lastfilledtweetvalue + 1; /// To give Two empty Spaces before staring new profile 
                                    ProcessExcel(OutputxlWorkBook, rowid, url);
                                }

                            }
                            else if (sitevalue == 5) // YELP
                            {
                                lastfilledtweetvalue = lastfilledtweetvalue + 1;
                                input = "";
                                // string  = "";//EMPTY TEMP FIX        (string)(xlRange.Cells[i, 1] as Excel.Range).Value2; // FEDRAL ID
                                string rowid = (string)(xlRange.Cells[i, 4] as Excel.Range).Value2;
                                // string state = (string)(xlRange.Cells[i, 5] as Excel.Range).Value2;

                                ProcessYelpExcel(OutputxlWorkBook, rowid);

                            }
                            else if (sitevalue == 7 || sitevalue == 8) // YELP  PRICE RANGE DATA
                            {
                                lastfilledtweetvalue = lastfilledtweetvalue + 1;
                                input = "";
                                string rowid = (string)(xlRange.Cells[i, 1] as Excel.Range).Value2;
                                ProcessYelpRestaurant(OutputxlWorkBook, rowid);
                            }

                            else
                            {
                                profile = split(url, ".com/")[1].ToLower(); // To  Take profile name from the url 
                                if (erroroccured)
                                    break;
                                atprofile = "@" + profile;
                                if (profile.Contains("@")) /// Specail Case need To have a special check 
                                    atprofile = profile;
                                lastfilledtweetvalue = lastfilledtweetvalue + 2; /// To give Two empty Spaces before staring new profile 
                                if (sitevalue == 0)
                                    ExcelMain(OutputxlWorkBook);
                                else
                                    ExcelFacebookMain(OutputxlWorkBook);
                            }
                        }
                        
                    }
                    catch
                    {
                        erroroccured = true;
                        OutputxlWorkBook.SaveAs("scrappingErr22.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                        js.ExecuteScript("alert('Error Occured');");
                        js.ExecuteScript("alert(valuelast);");
                        OutputxlWorkBook.Close(true, misValue, misValue);
                        xlApp.Quit();
                        //close and release
                        xlWorkbook.Close();
                        Marshal.ReleaseComObject(xlWorkbook);
                        //quit and release
                        xlApp.Quit();
                        Marshal.ReleaseComObject(xlApp);
                    }
                }

            }
            else
            {
                if (yf)
                    ProcessyahooFinance(OutputxlWorkBook);
                else
                    ProcessInsideAirbnb(OutputxlWorkBook);
            }
            if (!erroroccured)
            {
                OutputxlWorkBook.SaveAs("hashtag.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
               
                if (sitevalue != 4)
                {
                    js.ExecuteScript("alert('Data Scrapped');");

                }
            }
            //release com objects to fully kill excel process from running in the background
            
            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
         //   OutputxlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        #region for Processing yahoo Finance
        static void ProcessyahooFinance(Excel.Workbook xlWorkBook)
        {
            url = "https://finance.yahoo.com/quote/AAPL?p=AAPL";
            Excel.Worksheet xlWorkSheet;
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            count = 0;
            // Processing 
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(url);


            if (true)    // Page End is Found
            {
                // System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 second
                IWebElement element = driver.FindElement(By.Id("quote-summary"));
                xlWorkSheet = YahooExcelWrite(xlWorkSheet, element);
            }




        }
        #endregion

        #region For preparing Excel Sheet
        public static Excel.Worksheet YahooExcelWrite(Excel.Worksheet xlWorkSheet, IWebElement element)
        {
            string s = element.Text.ToString();
            string[] lines = Regex.Split(s, ",");
            xlWorkSheet.Cells[lastfilledtweetvalue, 1] = element.Text.ToString();
            for (int j = 0; j < lines.Count(); j++)
            {
                if (j % 2 == 0)
                    xlWorkSheet.Cells[lastfilledtweetvalue, 1] = lines[j];
                else
                {
                    xlWorkSheet.Cells[lastfilledtweetvalue, 2] = lines[j];
                    lastfilledtweetvalue = lastfilledtweetvalue + 1;
                }
            }

            return xlWorkSheet;
        }
        #endregion

        #region for Processing Inside Airbnb 
        static void ProcessInsideAirbnb(Excel.Workbook xlWorkBook)
        {
            url = "http://insideairbnb.com/get-the-data.html";
            Excel.Worksheet xlWorkSheet;
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            if (lastfilledtweetvalue == 1) // ADD HEADERS ONLY ONCE 
            {
                xlWorkSheet.Cells[1, 1] = "City";
                xlWorkSheet.Cells[1, 2] = "State";
                xlWorkSheet.Cells[1, 3] = "Country";
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            }
            count = 0;
            // Processing 
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(url);
            for (; ; ) //  Defined Infinite Loop and tiil the End of the Page is reached and then loop is terminated   
            {

                // Code to Scroll Page till the End 
                // 2000 pixel given randomly 
                int value = count * 2000;
                js.ExecuteScript("scrollBy(0,(" + value + "))");

                IWebElement elementend = driver.FindElement(By.CssSelector(".contentContainer>p>a>img"));  // Find the End of the Page 


                if (elementend.Displayed)    // Page End is Found
                {
                    // System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
                    List<IWebElement> element = driver.FindElements(By.CssSelector(".contentContainer>h2")).ToList();
                    xlWorkSheet = ExcelWrite(xlWorkSheet, element);
                    break;
                }


            }

        }
        #endregion

        #region for Yelp Processing 
        static void ProcessYelpExcel(Excel.Workbook xlWorkBook, string rowid)
        {
            Excel.Worksheet xlWorkSheet;
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            if (lastfilledtweetvalue == 2) // ADD HEADERS ONLY ONCE 
            {
                xlWorkSheet.Cells[1, 1] = "URL";
                xlWorkSheet.Cells[1, 2] = "Name";
                xlWorkSheet.Cells[1, 3] = "Number of Reviews ";
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            }
            count = 0;
            // Processing 
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(url);
            try
            {
                IWebElement elementend = driver.FindElement(By.CssSelector(".biz-page-header-left.claim-status"));
                xlWorkSheet.Cells[lastfilledtweetvalue, 1] = url;
                xlWorkSheet.Cells[lastfilledtweetvalue, 2] = rowid;
                xlWorkSheet.Cells[lastfilledtweetvalue, 3] = elementend.Text.ToString();
                driver.Close();
            }
            catch
            {
                xlWorkSheet.Cells[lastfilledtweetvalue, 1] = url;
                xlWorkSheet.Cells[lastfilledtweetvalue, 2] = input;
                xlWorkSheet.Cells[lastfilledtweetvalue, 3] = "";
                driver.Close();
            }
        }
        #endregion 

        #region for Yelp Processing 
        static void ProcessYelpRestaurant(Excel.Workbook xlWorkBook, string rowid)
        {
            Excel.Worksheet xlWorkSheet;
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            url = "https://www.yelp.com/biz/" + rowid;
            if (lastfilledtweetvalue == 2) // ADD HEADERS ONLY ONCE 
            {
                if (sitevalue == 7) // FOR PRICE RANGE DATA
                {
                    xlWorkSheet.Cells[1, 1] = "Name";
                    xlWorkSheet.Cells[1, 2] = "Price Range";
                }
                else
                {
                    xlWorkSheet.Cells[1, 1] = "review.unique.id";
                    xlWorkSheet.Cells[1, 2] = "restaurant.id";
                    xlWorkSheet.Cells[1, 3] = "review.user.name";
                    xlWorkSheet.Cells[1, 4] = "review.user.id";

                    xlWorkSheet.Cells[1, 5] = "review.user.location";
                    xlWorkSheet.Cells[1, 6] = "review.user.friends";
                    xlWorkSheet.Cells[1, 7] = "review.user.reviews";
                    xlWorkSheet.Cells[1, 8] = "review.user.elite";

                    xlWorkSheet.Cells[1, 9] = "review.rating";
                    xlWorkSheet.Cells[1, 10] = "review.date";
                    xlWorkSheet.Cells[1, 11] = "review.tags.checkin";
                    xlWorkSheet.Cells[1, 12] = "review.tags.first.to.review";

                    xlWorkSheet.Cells[1, 13] = "review.tag.favorites.lists";
                    xlWorkSheet.Cells[1, 14] = "review.tags.rotd";
                    xlWorkSheet.Cells[1, 15] = "review.text";
                    xlWorkSheet.Cells[1, 16] = "review.photos.number";

                    xlWorkSheet.Cells[1, 17] = "review.photos.text";
                    xlWorkSheet.Cells[1, 18] = "useful.vote.count";
                    xlWorkSheet.Cells[1, 19] = "funny.vote.count";
                    xlWorkSheet.Cells[1, 20] = "cool.vote.count";

                    xlWorkSheet.Cells[1, 21] = "review.comment";
                    xlWorkSheet.Cells[1, 22] = "identifier.business.owner";
                    // URL ADDING 
                    xlWorkSheet.Cells[1, 22] = "url"; // Checking Purpose 
                    lastfilledtweetvalue = lastfilledtweetvalue + 1;

                }
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            }
            count = 0;
            // Processing 
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(url);
            if (sitevalue == 7) // FOR PRICE RANGE DATA
            {
                try
                {
                    IWebElement elementend = driver.FindElement(By.CssSelector(".nowrap.price-description"));
                    xlWorkSheet.Cells[lastfilledtweetvalue, 1] = url;
                    xlWorkSheet.Cells[lastfilledtweetvalue, 2] = elementend.Text.ToString();
                    driver.Close();
                }
                catch
                {

                    xlWorkSheet.Cells[lastfilledtweetvalue, 1] = url;
                    xlWorkSheet.Cells[lastfilledtweetvalue, 2] = "";
                    driver.Close();
                }
            }
            else
            {
                // Review id 
                for (; ; ) //  Defined Infinite Loop and tiil the End of the Page is reached and then loop is terminated   
                {

                    // Code to Scroll Page till the End 
                    // 2000 pixel given randomly 
                    int value = count * 2000;
                    js.ExecuteScript("scrollBy(0,(" + value + "))");

                    IWebElement elementend = driver.FindElement(By.CssSelector(".page-of-pages.arrange_unit.arrange_unit--fill"));  // Find the End of the Page 


                    if (elementend.Displayed)    // Page End is Found
                    {
                        System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
                        xlWorkSheet = ExcelYelpReviews(xlWorkSheet, driver);
                        break;
                    }
                    count++;


                }

            }
        }
        #endregion

        #region for yelp Reviews 
        public static Excel.Worksheet ExcelYelpReviews(Excel.Worksheet xlWorkSheet, IWebDriver driver)
        {
            try
            {

                List<IWebElement> REVIEWS = driver.FindElements(By.CssSelector(".ylist.ylist-bordered.reviews>li")).ToList();
                List<IWebElement> reviewdateandrating = driver.FindElements(By.CssSelector(".biz-rating.biz-rating-large.clearfix")).ToList();
                List<IWebElement> reviewtext = driver.FindElements(By.CssSelector(".review-content>p")).ToList();
                List<IWebElement> votingbuttons = driver.FindElements(By.CssSelector(".voting-buttons")).ToList();
                for (int i = 0; i < REVIEWS.Count; i++)
                {
                    if (!string.IsNullOrEmpty(REVIEWS[i].Text))
                    {
                        string s = REVIEWS[i].Text.ToString();
                        string[] lines = Regex.Split(s, "\r\n");
                        if (lines.Count() != 1)
                        {
                            for (int j = 0; j < lines.Count(); j++)
                            {
                                xlWorkSheet.Cells[lastfilledvalue + 1, 3] = lines[0]; // Name
                                                                                      // xlWorkSheet.Cells[1, 4] = lines[0];
                                xlWorkSheet.Cells[lastfilledvalue + 1, 5] = lines[1];
                                // Further Processing need to be done 
                                xlWorkSheet.Cells[lastfilledvalue + 1, 6] = lines[2];
                                xlWorkSheet.Cells[lastfilledvalue + 1, 7] = lines[3];

                                xlWorkSheet.Cells[1, 8] = "review.user.elite";

                                // Special Condition Checking for 

                                string[] voting;

                                if (REVIEWS.Count == reviewtext.Count)
                                {
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 15] = reviewtext[i].Text;
                                    voting = Regex.Split(votingbuttons[i].Text.ToString(), "\r\n");

                                }
                                else
                                {
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 9] = reviewdateandrating[i - 1].Text;
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 10] = reviewdateandrating[i - 1].Text;
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 15] = reviewtext[i - 1].Text;
                                    voting = Regex.Split(votingbuttons[j - 1].Text.ToString(), "\r\n");

                                }
                                if (REVIEWS.Count == reviewtext.Count)
                                {
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 9] = reviewdateandrating[i - 1].Text;
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 10] = reviewdateandrating[i - 1].Text;

                                }
                                else
                                {
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 9] = reviewdateandrating[i].Text;
                                    xlWorkSheet.Cells[lastfilledvalue + 1, 10] = reviewdateandrating[i].Text;

                                }
                                xlWorkSheet.Cells[lastfilledvalue + 1, 18] = voting[1];
                                xlWorkSheet.Cells[lastfilledvalue + 1, 19] = voting[2];
                                xlWorkSheet.Cells[lastfilledvalue + 1, 20] = voting[3];

                                lastfilledvalue = lastfilledvalue + 1;
                                //xlWorkSheet.Cells[1, 9] = reviewdateandrating[j].Text;  //Over
                                //xlWorkSheet.Cells[1, 10] = "review.date"; // Over 

                                //xlWorkSheet.Cells[1, 11] = "review.tags.checkin";
                                //xlWorkSheet.Cells[1, 12] = "review.tags.first.to.review";

                                //xlWorkSheet.Cells[1, 13] = "review.tag.favorites.lists";
                                //xlWorkSheet.Cells[1, 14] = "review.tags.rotd";

                                //xlWorkSheet.Cells[1, 15] = "review.text";// Over 
                                //xlWorkSheet.Cells[1, 16] = "review.photos.number";

                                //xlWorkSheet.Cells[1, 17] = "review.photos.text";
                                //xlWorkSheet.Cells[1, 18] = "useful.vote.count"; // Over 

                                //xlWorkSheet.Cells[1, 19] = "funny.vote.count"; //Over
                                //xlWorkSheet.Cells[1, 20] = "cool.vote.count";// Over

                                //xlWorkSheet.Cells[1, 21] = "review.comment";
                                //xlWorkSheet.Cells[1, 22] = "identifier.business.owner";

                            }
                        }

                    }
                }
            }
            catch
            {

            }
            return xlWorkSheet;

        }
        #endregion 

        #region  For  Excel main  loop
        static void ExcelMain(Excel.Workbook xlWorkBook)
        {
            count = 0; Excel.Worksheet xlWorkSheet;
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(url);

            // For  Main Profile 
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            bool skip = true;
            if (!skip)
            {
                List<IWebElement> headerelement = driver.FindElements(By.CssSelector(".ProfileNav-list")).ToList(); //  Find item with Css  Class as  ProfileNav-list

                for (int i = 0; i < headerelement.Count; i++)
                {
                    if (!string.IsNullOrEmpty(headerelement[i].Text))
                    {
                        string s = headerelement[i].Text.ToString();
                        string[] lines = Regex.Split(s, "\r\n");
                        for (int j = 0; j < lines.Count() - 1; j++)
                        {
                            if (!CheckIntornot(lines[j]))
                            {

                                xlWorkSheet.Cells[lastfilledvalue + 1, 1] = lines[j];
                                xlWorkSheet.Cells[lastfilledvalue + 1, 2] = lines[j + 1];
                                lastfilledvalue = lastfilledvalue + 1;
                            }
                        }

                    }
                }
                headerelement = driver.FindElements(By.CssSelector(".ProfileHeaderCard")).ToList();  //  Find item with Css  Class as  ProfileHeaderCard
                for (int i = 0; i < headerelement.Count; i++)
                {
                    if (!string.IsNullOrEmpty(headerelement[i].Text))
                    {
                        string s = headerelement[i].Text.ToString();
                        string[] lines = Regex.Split(s, "\r\n");
                        profilename = lines[0];// To take Profile name from here ratherthan url 
                        for (int j = 0; j < lines.Count(); j++)
                        {

                            xlWorkSheet.Cells[lastfilledvalue + 1, 1] = lines[j];
                            lastfilledvalue = lastfilledvalue + 1;

                        }

                    }
                }
                headerelement = driver.FindElements(By.CssSelector(".PhotoRail-headingWithCount.js-nav")).ToList(); //  Find item with Css  Class as  PhotoRail-headingWithCount.js-nav
                for (int i = 0; i < headerelement.Count; i++)
                {
                    if (!string.IsNullOrEmpty(headerelement[i].Text))
                    {
                        string s = headerelement[i].Text.ToString();
                        string[] lines = Regex.Split(s, "\r\n");
                        for (int j = 0; j < lines.Count(); j++)
                        {

                            xlWorkSheet.Cells[lastfilledvalue + 1, 1] = lines[j];
                            lastfilledvalue = lastfilledvalue + 1;

                        }

                    }
                }
            }

            //  For Tweets and replies 
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);// Take Sheet 2  For filling twwets and Replies 
            DateTime Endtime = DateTime.Now.AddMinutes(60);
            for (; ; ) //  Defined Infinite Loop and tiil the End of the Page is reached and then loop is terminated   
            {

                // Code to Scroll Page till the End 
                // 2000 pixel given randomly 
                int value = count * 5000;
                js.ExecuteScript("scrollBy(0,(" + value + "))");

                if (Endtime<DateTime.Now)    // Page End is Found
                {
                    System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
                    List<IWebElement> element = driver.FindElements(By.CssSelector(".tweet.js-stream-tweet.js-actionable-tweet.js-profile-popup-actionable.original-tweet.js-original-tweet")).ToList();

                    // RAVI
                    if (hashtag)
                        xlWorkSheet = HashtagExcelWrite(xlWorkSheet, element);
                    else
                        xlWorkSheet = ExcelWrite(xlWorkSheet, element);
                    break;
                }
                count++;


            }

            //  driver.Close(); TEMP SETTING TO SEE THE DATA 

        }
        #endregion

        #region For preparing Excel Sheet
        public static Excel.Worksheet HashtagExcelWrite(Excel.Worksheet xlWorkSheet, List<IWebElement> element)
        {


            // For Headings to the Excel Sheet 
            xlWorkSheet.Cells[1, 1] = "Date";
            xlWorkSheet.Cells[1, 2] = "Account Name";
            xlWorkSheet.Cells[1, 3] = "Tweet";
            xlWorkSheet.Cells[1, 4] = "";
            xlWorkSheet.Cells[1, 5] = "";
            xlWorkSheet.Cells[1, 6] = "Reply";
            xlWorkSheet.Cells[1, 7] = "Retweet";
            xlWorkSheet.Cells[1, 8] = "Like";
            xlWorkSheet.Cells[1, 9] = "Url";


            for (int i = 0; i < element.Count; i++)
            {
                if (i > -1)
                {
                    postedtoday = false;
                    string s = element[i].Text.ToString();
                    string[] lines = Regex.Split(s, "\r\n");
                    // Temp Setting 
                    //if (lastfilledtweetvalue == 60)
                    //{
                    //    s = element[i].Text.ToString();
                    //}
                    for (int j = 0; j < lines.Count(); j++)
                    {
                        if (lines[j].Equals("Reply"))
                        {
                            replypos = j;
                        }
                        if (lines[j].Equals("Retweet"))
                        {
                            retweetpos = j;
                        }
                        if (lines[j].Equals("Like"))
                        {
                            likepos = j;
                        }
                        if (lines[j].Equals("More"))
                        {
                            morepos = j;

                        }

                        maxcount = lines.Count();

                    }
                    if (replypos + 1 == retweetpos)
                        xlWorkSheet.Cells[lastfilledtweetvalue, 6] = 0;
                    else
                        xlWorkSheet.Cells[lastfilledtweetvalue, 6] = lines[replypos + 1];   // Number of Replies,if there are no replies then Reply will be showed.
                    if (retweetpos + 1 == likepos)
                        xlWorkSheet.Cells[lastfilledtweetvalue, 7] = 0;
                    else
                        xlWorkSheet.Cells[lastfilledtweetvalue, 7] = lines[retweetpos + 1];   // Number of Retweets     
                    if (maxcount - 1 == likepos)
                        xlWorkSheet.Cells[lastfilledtweetvalue, 8] = 0;
                    else
                        xlWorkSheet.Cells[lastfilledtweetvalue, 8] = lines[maxcount - 1];   //  Number of  Likes
                    xlWorkSheet.Cells[lastfilledtweetvalue, 9] = url;   // For url 
                    string Tweet = "";
                    xlWorkSheet.Cells[lastfilledtweetvalue, 2] = profile.ToUpper();
                    int startvalue = 2;
                    startvalue = morepos + 1; // Modified 
                    for (int r = startvalue; r < replypos; r++) // This is due if they give break line inbetween,then to handle 
                    {
                        Tweet += "";
                        Tweet += lines[r].ToString();
                    }
                    xlWorkSheet.Cells[lastfilledtweetvalue, 3] = Tweet;
                    for (int r = 0; r < (startvalue - 1); r++) // This is due if they give break line inbetween,then to handle 
                    {
                        Tweet = "";
                        Tweet += lines[r].ToString();
                    }
                    xlWorkSheet.Cells[lastfilledtweetvalue, 2] = Tweet;

                    lastfilledtweetvalue = lastfilledtweetvalue + 1;
                }
            }

            return xlWorkSheet;

        }
        #endregion

        #region For preparing Excel Sheet
        public static Excel.Worksheet ExcelWrite(Excel.Worksheet xlWorkSheet, List<IWebElement> element)
        {
            if (sitevalue == 6) // INSIDE AIR BNB
            {
                for (int i = 0; i < element.Count; i++)
                {
                    string s = element[i].Text.ToString();
                    string[] lines = Regex.Split(s, ",");
                    xlWorkSheet.Cells[lastfilledtweetvalue, 1] = element[i].Text.ToString();
                    for (int j = 0; j < lines.Count(); j++)
                    {
                        xlWorkSheet.Cells[lastfilledtweetvalue, j + 1] = lines[j];
                    }
                    lastfilledtweetvalue = lastfilledtweetvalue + 1;
                }
                return xlWorkSheet;
            }
            if (sitevalue == 1)
            {
                for (int i = 0; i < element.Count; i++)
                {
                    xlWorkSheet.Cells[lastfilledtweetvalue, 1] = element[i].Text.ToString();
                    lastfilledtweetvalue = lastfilledtweetvalue + 1;
                }
                return xlWorkSheet;
            }
            // For Headings to the Excel Sheet 
            xlWorkSheet.Cells[1, 1] = "Date";
            xlWorkSheet.Cells[1, 2] = "Account Name";
            xlWorkSheet.Cells[1, 3] = "Tweet";
            xlWorkSheet.Cells[1, 4] = "Org  Account Name";
            xlWorkSheet.Cells[1, 5] = "Org Tweet Content";
            xlWorkSheet.Cells[1, 6] = "Reply";
            xlWorkSheet.Cells[1, 7] = "Retweet";
            xlWorkSheet.Cells[1, 8] = "Like";
            xlWorkSheet.Cells[1, 9] = "Url";


            for (int i = 0; i < element.Count; i++)
            {
                if (i > -1)
                {
                    postedtoday = false;
                    string s = element[i].Text.ToString();
                    string[] lines = Regex.Split(s, "\r\n");
                    // Temp Setting 
                    //if (lastfilledtweetvalue == 60)
                    //{
                    //    s = element[i].Text.ToString();
                    //}
                    for (int j = 0; j < lines.Count(); j++)
                    {
                        if (lines[j].Equals("Reply"))
                        {
                            replypos = j;
                        }
                        if (lines[j].Equals("Retweet"))
                        {
                            retweetpos = j;
                        }
                        if (lines[j].Equals("Like"))
                        {
                            likepos = j;
                        }
                        if (lines[j].Equals("More"))
                        {
                            morepos = j;
                            if (Case == 1 || Case == 5)
                            { // Extra Checking 
                                if (!lines[morepos - 1].Contains("@"))
                                    postedtoday = true;
                            }
                            else if (Case == 3)
                            {
                                if (!CheckhasMonth(lines[morepos - 1]))
                                    postedtoday = true;
                                else
                                    postedtoday = false;

                            }
                            else if (Case == 0)
                                if (!CheckhasMonth(lines[morepos - 1]))
                                    postedtoday = true;
                                else
                                    postedtoday = false;
                        }

                        maxcount = lines.Count();
                        if (j == 0)  // One Time Verificaton finding the case 
                        {
                            postedtoday = false;
                            if (lines[0].Contains("Retweeted"))
                            {
                                Case = 1;
                                if (CheckhasMonth(lines[1]))
                                    postedtoday = true;
                            }
                            else if (lines[0].Contains("Pinned Tweet"))
                            {

                                Case = 3;
                                if (!CheckhasMonth(lines[1]))
                                    postedtoday = true;


                            }
                            else if (lines[0].Contains("In reply to"))
                            {
                                postedtoday = false;
                                if (!CheckhasMonth(lines[1]))
                                    postedtoday = true;

                                Case = 4;

                            }
                            else
                            {
                                Case = 0;
                                if (!CheckhasMonth(lines[0]))
                                    postedtoday = true;

                            }

                        }
                        if (j > 0)
                        {
                            if (lines[j].Contains("Retweeted"))
                            {

                                Case = 2;
                                retweetedpos = j;
                                if (!CheckhasMonth(lines[0]))
                                    postedtoday = true;
                                if (lines[0].Contains("Retweeted"))
                                {
                                    Case = 5;
                                    if (!CheckhasMonth(lines[1]))
                                        postedtoday = true;
                                }
                            }

                        }

                        if (Case == 2 || Case == 5)
                        {
                            string checking = new CultureInfo("en-US").TextInfo.ToTitleCase(profilename) + " " + "added,";
                            if (lines[j].Contains(checking))
                            {
                                if (lines[j + 1].Contains("@"))
                                    profilepos = j + 1;
                                else
                                    profilepos = j + 2;
                            }

                        }


                    }
                    if (replypos + 1 == retweetpos)
                        xlWorkSheet.Cells[lastfilledtweetvalue, 6] = 0;
                    else
                        xlWorkSheet.Cells[lastfilledtweetvalue, 6] = lines[replypos + 1];   // Number of Replies,if there are no replies then Reply will be showed.
                    if (retweetpos + 1 == likepos)
                        xlWorkSheet.Cells[lastfilledtweetvalue, 7] = 0;
                    else
                        xlWorkSheet.Cells[lastfilledtweetvalue, 7] = lines[retweetpos + 1];   // Number of Retweets     
                    if (maxcount - 1 == likepos)
                        xlWorkSheet.Cells[lastfilledtweetvalue, 8] = 0;
                    else
                        xlWorkSheet.Cells[lastfilledtweetvalue, 8] = lines[maxcount - 1];   //  Number of  Likes
                    xlWorkSheet.Cells[lastfilledtweetvalue, 9] = url;   // For url 
                    string Tweet = "";
                    xlWorkSheet.Cells[lastfilledtweetvalue, 2] = profile.ToUpper();
                    int startvalue = 2;
                    if (Case == 0 || Case == 3 || Case == 4)
                    {
                        if (postedtoday || Case == 3 || Case == 4 || Case == 0)
                            startvalue = morepos + 1; // Modified 

                        for (int r = startvalue; r < replypos; r++) // This is due if they give break line inbetween,then to handle 
                        {
                            Tweet += "";
                            Tweet += lines[r].ToString();
                        }
                        xlWorkSheet.Cells[lastfilledtweetvalue, 3] = Tweet;
                        if (Case == 4)
                            xlWorkSheet.Cells[lastfilledtweetvalue, 4] = split(lines[0].ToString(), "In reply to")[1];

                    }

                    else if (Case == 1)
                    {
                        startvalue = 3;
                        if (postedtoday)
                            startvalue = 4;
                        // Check special case
                        startvalue = morepos + 1;
                        for (int r = startvalue; r < replypos; r++)
                        {
                            Tweet += "";
                            Tweet += lines[r].ToString();
                        }
                        xlWorkSheet.Cells[lastfilledtweetvalue, 3] = Tweet;
                        // xlWorkSheet.Cells[lastfilledtweetvalue, 4] = lines[1];
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = lines[morepos - 1];
                        if (postedtoday)
                            xlWorkSheet.Cells[lastfilledtweetvalue, 4] = lines[morepos - 2];
                        xlWorkSheet.Cells[lastfilledtweetvalue, 5] = Tweet;

                    }
                    else if (Case == 2 || Case == 5) // RAVI 
                    {
                        xlWorkSheet.Cells[lastfilledtweetvalue, 3] = lines[retweetedpos + 1];
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = lines[profilepos]; // Orginal Account
                        for (int r = profilepos + 1; r < replypos; r++) // Orginial  Account Tweet 
                        {
                            Tweet += "";
                            Tweet += lines[r].ToString();
                        }
                        xlWorkSheet.Cells[lastfilledtweetvalue, 5] = Tweet;


                    }
                    if (!postedtoday)
                    {
                        string date = "";
                        Addyear = false;
                        if (Case == 1 || Case == 5) // RAVI 
                        {
                            string[] dates = split(lines[morepos - 1].ToString(), " ");// Special Case 
                            if (dates[dates.Count() - 1].ToString().Length > 2) // If it Contains  year also in the Date.
                            {
                                date = dates[dates.Count() - 3].ToString();
                                date += " ";
                                date += dates[dates.Count() - 2].ToString();
                            }
                            else
                            {
                                date = dates[dates.Count() - 2].ToString();
                                Addyear = true;
                            }
                            date += " ";
                            date += dates[dates.Count() - 1].ToString();
                            if (Addyear)
                                date += " 2017"; // Static Value giving present year 

                            xlWorkSheet.Cells[lastfilledtweetvalue, 1] = Convert.ToDateTime(date);

                        }
                        else
                        {
                            if (Case == 4)
                                date = split(lines[1].ToString().ToLower(), atprofile)[1];
                            if (Case == 3 || Case == 0)
                                date = split(lines[morepos - 1].ToString().ToLower(), atprofile)[1];
                            else
                                date = split(lines[0].ToString().ToLower(), atprofile)[1];
                            string[] dates = split(date, " ");
                            if (dates.Count() == 3) // If it  Doesnot Contains year in the Date.
                            {
                                Addyear = true;
                            }
                            // EXtra checking for the yaar
                            String Date = dates[dates.Count() - 1].ToString();
                            if (Date.Length == 4)
                                Addyear = false;

                            if (Addyear)
                                date += " 2017"; // Static Value giving present year 

                            xlWorkSheet.Cells[lastfilledtweetvalue, 1] = Convert.ToDateTime(date);



                        }

                    }
                    else
                    { // As it is Poested today no date will be there Date is  added 
                        lines[0] = " ";

                        xlWorkSheet.Cells[lastfilledtweetvalue, 1] = DateTime.UtcNow.ToString("MM/dd/yyyy");



                    }
                    lastfilledtweetvalue = lastfilledtweetvalue + 1;
                }
            }

            return xlWorkSheet;

        }
        #endregion

        #region for Checking Whether it is number or string
        public static bool CheckIntornot(string a)
        {
            if (a.Any(char.IsDigit))
                return true;
            else
                return false;

        }
        #endregion

        #region for Checking whether it has month name i.e Date 
        public static bool CheckhasMonth(string a)
        {

            string[] substr = Regex.Split(a.ToLower(), atprofile);
            string s;
            if (Case != 1)
            {
                if (substr.Count() > 1)
                    s = substr[1].ToLower();
                else
                    s = a.ToLower();
                if (s.Contains("jan") || s.Contains("feb") || s.Contains("mar") || s.Contains("apr") || s.Contains("may") || s.Contains("jun") || s.Contains("jul") || s.Contains("aug") || s.Contains("sep") || s.Contains("oct") || s.Contains("nov") || s.Contains("dec"))
                    return true;
                else

                    return false;
            }
            return false;
        }
        #endregion

        #region for Spliting a string  at specific charcter
        public static string[] split(string content, string splitvalue)
        {

            string[] substr = Regex.Split(content, splitvalue);
            return substr;

        }

        #endregion

        #region For Facebook 
        static void ExcelFacebookMain(Excel.Workbook xlWorkBook)
        {
            count = 0; Excel.Worksheet xlWorkSheet;
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(url);

            // For  Main Profile 
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            List<IWebElement> headerelement = new List<IWebElement>();
            IWebElement elementend = null;          //driver.FindElement(By.CssSelector(".//*[@id='www_pages_reaction_see_more_unit']/div/a"));  // Find the End of the Page 
            for (; ; ) //  Defined Infinite Loop and tiil the End of the Page is reached and then loop is terminated   
            {
                int value = count * 2000;
                js.ExecuteScript("scrollBy(0,(" + value + "))");
                if (elementend != null)    // Page End is Found
                {
                    System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
                    headerelement = driver.FindElements(By.CssSelector("._1dwg._1w_m._2ph_")).ToList(); //  Find item with Css  Class as  ProfileNav-list
                    xlWorkSheet = ExcelWrite(xlWorkSheet, headerelement);
                    break;

                }
                count++;
            }
            driver.Close();
        }
        #endregion

        #region For Process Excel 
        static void ProcessExcel(Excel.Workbook xlWorkBook, int Rowid, string amenities)
        {
            Excel.Worksheet xlWorkSheet;
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            // For Headings to the Excel Sheet 
            if (lastfilledtweetvalue == 2) // ADD HEADERS ONLY ONCE 
            {
                xlWorkSheet.Cells[1, 1] = "Property ID";
                xlWorkSheet.Cells[1, 2] = " Amenities Count";
            }

            amenities = amenities.Replace("{", "");
            amenities = amenities.Replace("}", "");
            amenities = amenities.Replace("\"", "");
            string[] lines = Regex.Split(amenities, ",");
            int ameneitiescount = lines.Count();
            xlWorkSheet.Cells[lastfilledtweetvalue, 1] = Rowid;
            xlWorkSheet.Cells[lastfilledtweetvalue, 2] = ameneitiescount;
            // RAVI 


            xlWorkSheet.Cells[lastfilledtweetvalue, 2] = ameneitiescount;
            for (int i = 0; i < ameneitiescount; i++)
            {
                if (myCol.Contains(lines[i]))
                {
                    myCol.IndexOf(lines[i]);
                    xlWorkSheet.Cells[lastfilledtweetvalue, myCol.IndexOf(lines[i]) + 3] = 1;
                }
                else
                {
                    myCol.Add(lines[i]);
                    xlWorkSheet.Cells[1, myCol.IndexOf(lines[i]) + 3] = lines[i]; // TO ADD HEADING 
                    xlWorkSheet.Cells[lastfilledtweetvalue, myCol.IndexOf(lines[i]) + 3] = 1;
                }
            }

        }
        #endregion


        #region For TripAdivsor Main Review Content 
        static void ExcelTripReviewAdivsorMain(Excel.Workbook xlWorkBook)
        {
            count = 0; Excel.Worksheet xlWorkSheet;
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            //  driver.Navigate().GoToUrl(url);
            driver.Navigate().GoToUrl("https://www.tripadvisor.com/Hotel_Review-g55197-d775880-Reviews-River_Inn_of_Harbor_Town-Memphis_Tennessee.html#REVIEWS");
            // For  Main Profile 
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //  
            bool processlastime = false;
            List<IWebElement> ratingelement = new List<IWebElement>();



            for (; ; ) //  Defined Infinite Loop For paging  
            {
                IWebElement paging = driver.FindElement(By.CssSelector(".unified.pagination"));
                if (IsElementPresent(paging, ".nav.next.disabled"))
                    processlastime = true;

                for (; ; ) //  Defined Infinite Loop and tiil the End of the Page is reached and then loop is terminated   
                {
                    // System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
                    int value = count * 2000;
                    js.ExecuteScript("scrollBy(0,(" + value + "))");
                    IWebElement elementend = driver.FindElement(By.CssSelector(".unified.pagination"));  // Find The pagination       //driver.FindElement(By.CssSelector(".//*[@id='www_pages_reaction_see_more_unit']/div/a"));  // Find the End of the Page
                    if (elementend.Displayed)    // Page End is Found
                    {
                        // Processing 
                        IWebElement morebuttton = driver.FindElement(By.CssSelector(".taLnk.ulBlueLinks"));
                        morebuttton.Click();
                        System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds

                        ratingelement = driver.FindElements(By.CssSelector(".review.basic_review.inlineReviewUpdate.provider0")).ToList(); //  Find item with Css  Class as  ProfileNav-list
                                                                                                                                           //   headerelement = driver.FindElements(By.CssSelector(".property_details.easyClear")).ToList(); //  Find item with Css  Class as  ProfileNav-list
                        xlWorkSheet = ExcelTripAdivisorReviewWrite(xlWorkSheet, ratingelement);
                        break;

                    }
                    count++;
                }
                if (processlastime)
                {
                    driver.Close();
                    break;
                }
                else
                {  //
                    IWebElement nextbutton = driver.FindElement(By.CssSelector(".nav.next.rndBtn.ui_button.primary.taLnk"));
                    nextbutton.Click();
                }

            }

        }
        #endregion

        #region For The Trip Adivisor 
        public static Excel.Worksheet ExcelTripAdivisorReviewWrite(Excel.Worksheet xlWorkSheet, List<IWebElement> ratingelement)
        {
            // For Headings to the Excel Sheet 
            //xlWorkSheet.Cells[1, 1] = "Hotel Name";
            //xlWorkSheet.Cells[1, 2] = "Number OF Reviews";
            //xlWorkSheet.Cells[1, 3] = "Ranking";
            //xlWorkSheet.Cells[1, 4] = "Rating";
            //xlWorkSheet.Cells[1, 5] = "First Review";
            //xlWorkSheet.Cells[1, 6] = "1 Review Content";
            //xlWorkSheet.Cells[1, 7] = "Second Review";
            //xlWorkSheet.Cells[1, 8] = "2 Review Content";

            //xlWorkSheet.Cells[1, 9] = "Hotel Category";

            //      xlWorkSheet.Cells[1, 6] = "Reply";
            //xlWorkSheet.Cells[1, 7] = "Retweet";
            //xlWorkSheet.Cells[1, 8] = "Like";
            //xlWorkSheet.Cells[1, 9] = "Url";
            for (int i = 0; i < ratingelement.Count; i++)
            {
                string s = ratingelement[i].Text.ToString();
                string[] lines = Regex.Split(s, "\r\n");
                int columnvalue = 0;
                // if (lines[0] != "Sponsored") // To Process Only if it is Not Sponsored
                //{
                lastfilledtweetvalue = lastfilledtweetvalue + 1;

                for (int j = 0; j < lines.Count(); j++)
                {
                    if (lines[j] != "" && lines[j] != null) // Process Only if it is not Empty or Not Null
                    {
                        columnvalue = columnvalue + 1;
                        if (columnvalue == 4) // AS Added REview 
                            columnvalue = columnvalue + 1;
                        xlWorkSheet.Cells[lastfilledtweetvalue, columnvalue] = lines[j];

                    }

                }
                //}
            }
            return xlWorkSheet;
        }
        #endregion

        #region For TripAdivsor MainContent 
        static void ExcelTripAdivsorMain(Excel.Workbook xlWorkBook)
        {
            count = 0; Excel.Worksheet xlWorkSheet;
            IWebDriver driver = new ChromeDriver();
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl("https://www.tripadvisor.com");
            // For  Main Profile 
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            List<IWebElement> headerelement = new List<IWebElement>();
            List<IWebElement> ratingelement = new List<IWebElement>();
            // Processing 
            IWebElement searchbox = driver.FindElement(By.CssSelector("#searchbox"));
            searchbox.SendKeys(url);
            // System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
            System.Threading.Thread.Sleep(10000);
            //  js.ExecuteScript("$('date_picker_in_1').value='04/06/2017'");
            // js.ExecuteScript("$('date_picker_out_1').value='04/07/2017'");
            //  IWebElement startdate = driver.FindElement(By.CssSelector("#date_picker_in_1"));
            //   startdate.Clear();
            //  startdate.Text = "04/05/2017";
            //  startdate.SendKeys("04052017");
            ////  startdate.SendKeys(Keys.Tab);
            //   System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
            // IWebElement enddate = driver.FindElement(By.CssSelector("#date_picker_out_1"));
            //  enddate.SendKeys("04062017");
            //  enddate.SendKeys(Keys.Tab);
            IWebElement submit = driver.FindElement(By.CssSelector("#SUBMIT_HOTEL"));
            submit.Click();
            for (; ; ) //  Defined Infinite Loop and tiil the End of the Page is reached and then loop is terminated   
            {
                System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds
                int value = count * 2000;
                js.ExecuteScript("scrollBy(0,(" + value + "))");///.
                // IWebElement elementend = driver.FindElement(By.CssSelector(".unified.pagination.standard_pagination"));  // Find The pagination       //driver.FindElement(By.CssSelector(".//*[@id='www_pages_reaction_see_more_unit']/div/a"));  // Find the End of the Page
                IWebElement elementend = driver.FindElement(By.CssSelector(".disclaimer"));  // Find the Discalimar at the end 
                if (elementend.Displayed)    // Page End is Found
                {
                    System.Threading.Thread.Sleep(10000); // Wait Till  page is loaded in order to Scrap the Content // Time Wait is 10 seconds

                    ratingelement = driver.FindElements(By.CssSelector(".listing_rating")).ToList(); //  Find item with Css  Class as  ProfileNav-list
                    headerelement = driver.FindElements(By.CssSelector(".property_details.easyClear")).ToList(); //  Find item with Css  Class as  ProfileNav-list
                    xlWorkSheet = ExcelTripAdivisorWrite(xlWorkSheet, headerelement, ratingelement);
                    break;

                }
                count++;
            }
            driver.Close();
        }
        #endregion

        public static bool IsElementPresent(IWebElement element, string Cssname)
        {
            try
            {
                element.FindElement(By.CssSelector(Cssname));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        #region For The Trip Adivisor 
        public static Excel.Worksheet ExcelTripAdivisorWrite(Excel.Worksheet xlWorkSheet, List<IWebElement> element, List<IWebElement> ratingelement)
        {
            // For Headings to the Excel Sheet 
            xlWorkSheet.Cells[1, 1] = "Hotel Name";
            xlWorkSheet.Cells[1, 2] = "Number OF Reviews";
            xlWorkSheet.Cells[1, 3] = "Ranking";
            xlWorkSheet.Cells[1, 4] = "Rating";
            xlWorkSheet.Cells[1, 5] = "First Review";
            xlWorkSheet.Cells[1, 6] = "1 Review Content";
            xlWorkSheet.Cells[1, 7] = "Second Review";
            xlWorkSheet.Cells[1, 8] = "2 Review Content";

            xlWorkSheet.Cells[1, 9] = "Hotel Category";

            //      xlWorkSheet.Cells[1, 6] = "Reply";
            //xlWorkSheet.Cells[1, 7] = "Retweet";
            //xlWorkSheet.Cells[1, 8] = "Like";
            //xlWorkSheet.Cells[1, 9] = "Url";
            for (int i = 0; i < element.Count; i++)
            {
                string s = element[i].Text.ToString();
                string[] lines = Regex.Split(s, "\r\n");
                int columnvalue = 0;
                if (lines[0] != "Sponsored") // To Process Only if it is Not Sponsored
                {
                    lastfilledtweetvalue = lastfilledtweetvalue + 1;
                    //.ui_bubble_rating.bubble_45  // RAting 
                    if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_50"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "5.0";
                    if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_45"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "4.5";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_40"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "4.0";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_35"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "3.5";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_30"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "3.0";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_25"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "2.5";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_20"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "2.0";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_15"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "1.5";
                    else if (IsElementPresent(element[i], ".ui_bubble_rating.bubble_10"))
                        xlWorkSheet.Cells[lastfilledtweetvalue, 4] = "1.0";

                    //if (IsElementPresent(element[i], ".providerLogo > img"))
                    //{
                    //   IWebElement provider= element[i].FindElement(By.XPath(".//*[@id='hotel_775880']/div/div/div/div/div[4]/div/div[1]/div[1]/img"));
                    //   String img_text = provider.FindElement(By.TagName("img")).GetAttribute("alt");
                    //}

                    for (int j = 0; j < lines.Count(); j++)
                    {
                        if (lines[j] != "" && lines[j] != null && lines[j] != "/ night" && lines[j] != "View Deal") // Process Only if it is not Empty or Not Null
                        {
                            columnvalue = columnvalue + 1;
                            if (columnvalue == 4) // AS Added REview 
                                columnvalue = columnvalue + 1;
                            xlWorkSheet.Cells[lastfilledtweetvalue, columnvalue] = lines[j];

                        }

                    }
                }
            }
            return xlWorkSheet;
        }
        #endregion


    }

}
